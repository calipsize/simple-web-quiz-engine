package engine.dao;

import engine.module.Feedback;
import engine.module.Quiz;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public interface QuizIn {
    Quiz postNewQuiz(Quiz quiz);

    Quiz getQuizById(int id);

    List<Quiz> getAllQuizzes();

    Feedback getFeedback(int id, int answer);
}
