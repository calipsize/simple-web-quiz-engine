package engine.dao;

import engine.exceptions.NotFoundRequestException;
import engine.module.Feedback;
import engine.module.Quiz;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import static engine.module.Quiz.ID_COUNTER;


@Repository("mainRealisationQuizDao")
public class QuizDao implements QuizIn {
    private final List<Quiz> quizzesHolder = new ArrayList<>();

    @Override
    public Quiz postNewQuiz(Quiz quiz) {
        quiz.setId(++ID_COUNTER);
        quizzesHolder.add(quiz);
        return quiz;
    }

    @Override
    public List<Quiz> getAllQuizzes() {
        return quizzesHolder;
    }

    @Override
    public Quiz getQuizById(int id) {
        return quizzesHolder.stream()
                .filter(quiz -> quiz.getId() == id)
                .findFirst()
                .orElseThrow(
                        () -> new NotFoundRequestException("Not found!"));
    }

    public Feedback getFeedback(int id, int answer) {
        getQuizById(id);

        Feedback tempFeedback = new Feedback();

        // ifs for nice work

        if (quizzesHolder.get(id - 1).getAnswer() == answer) {
            tempFeedback.setSuccess(true);
            tempFeedback.setFeedback("Congratulations, you're right!");
        } else {
            tempFeedback.setSuccess(false);
            tempFeedback.setFeedback("Wrong answer! Please, try again.");
        }

        return tempFeedback;
    }
}
