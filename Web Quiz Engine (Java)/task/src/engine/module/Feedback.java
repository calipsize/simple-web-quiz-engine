package engine.module;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Feedback {

    @JsonProperty("success")
    private boolean success;

    @JsonProperty("feedback")
    private String feedback;

    public Feedback(Long answer) {
        if (answer == 2) {
            this.success = true;
            this.feedback = "Congratulations, you're right!";
        } else {
            this.success = false;
            this.feedback = "Wrong answer! Please, try again.";
        }
    }
}
