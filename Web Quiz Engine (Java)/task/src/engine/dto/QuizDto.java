package engine.dto;

import engine.module.Quiz;
import lombok.Getter;

import java.util.List;

@Getter
public class QuizDto {
    private final int id;
    private final String title;
    private final String text;
    private final List<String> options;

    public QuizDto(Quiz quiz) {
        this.id = quiz.getId();
        this.title = quiz.getTitle();
        this.text = quiz.getText();
        this.options = quiz.getOptions();
    }
}
