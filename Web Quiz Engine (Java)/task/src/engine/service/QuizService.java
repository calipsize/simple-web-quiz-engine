package engine.service;

import engine.dao.QuizIn;
import engine.dto.QuizDto;
import engine.module.Feedback;
import engine.module.Quiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuizService {
    private final QuizIn quizIn;

    @Autowired
    public QuizService(@Qualifier("mainRealisationQuizDao") QuizIn quizIn) {
        this.quizIn = quizIn;
    }

    public QuizDto postNewQuizService(Quiz quiz) {
        return new QuizDto(quizIn.postNewQuiz(quiz));
    }

    public List<QuizDto> getAllQuizzesService() {
        return quizIn.getAllQuizzes().stream().map(QuizDto::new).toList();
    }

    public QuizDto getQuizByIdService(int id) {
        return new QuizDto(quizIn.getQuizById(id));
    }

    public Feedback getFeedbackService(int id, int answer) {
        return quizIn.getFeedback(id, answer);
    }
}
