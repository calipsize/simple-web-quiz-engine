package engine.api;

import engine.dto.QuizDto;
import engine.module.Feedback;
import engine.module.Quiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import engine.service.QuizService;

import java.util.List;


@RestController
@RequestMapping("/api")
public class QuizController {
    private final QuizService quizService;

    @Autowired
    public QuizController(QuizService quizService) {
        this.quizService = quizService;
    }

    @PostMapping("/quizzes")
    public ResponseEntity<QuizDto> postNewQuiz(@RequestBody Quiz quiz) {
        return ResponseEntity.status(HttpStatus.OK).body(quizService.postNewQuizService(quiz));
    }

    @GetMapping("/quizzes")
    public ResponseEntity<List<QuizDto>> getAllQuizzes() {
        return ResponseEntity.status(HttpStatus.OK).body(quizService.getAllQuizzesService());
    }

    @GetMapping("/quizzes/{id}")
    public QuizDto getQuizById(@PathVariable int id) {
        return quizService.getQuizByIdService(id);
    }

    @PostMapping("/quizzes/{id}/solve")
    public Feedback postAnswer(@PathVariable int id, @RequestParam int answer) {
        return quizService.getFeedbackService(id, answer);
    }
}
